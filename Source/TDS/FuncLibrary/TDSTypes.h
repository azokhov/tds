// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "CoreMinimal.h"
#include <Kismet/BlueprintFunctionLibrary.h>
#include "TDSTypes.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName = "Aim State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State"),
	FastRun_State UMETA(DisplayName = "FastRun State"),
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
	float AimSpeed = 211.8f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
	float WalkSpeed = 306.3f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
	float RunSpeed = 412.1f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
	float FastRunSpeed = 821.4f;
};

UCLASS()
class TDS_API UTDSTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

};
