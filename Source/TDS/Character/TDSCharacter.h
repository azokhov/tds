// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS/FuncLibrary/TDSTypes.h"
#include "TDSCharacter.generated.h"

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATDSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void BeginPlay() override;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
		class UInputMappingContext* CharacterMappingContext;

	/** Move Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
		class UInputAction* MoveAction;	
	/** Scrol zoom Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
		class UInputAction* ZoomAction;
	/** Aim Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
		class UInputAction* AimAction;
	/** Walk Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
		class UInputAction* WalkAction;
	/** Fast Run Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
		class UInputAction* FastRunAction;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

public:

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Camera")
		float ZoomStep = 25.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Camera")
		float Smooth = 5.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Camera")
		float CameraHeightMax = 1100.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Camera")
		float CameraHeightMin = 700.0f;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "Movement")
		bool bIsAim = false;
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "Movement")
		bool bIsWalk = false;
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "Movement")
		bool bIsFastRun = false;
	FTimerHandle EnergyChargeTimer;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Energy")
		float RateEnergyCharge = 1.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Energy")
		float MaxEnergy = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Energy")
		float Energy = MaxEnergy;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Energy")
		float EnergyEnough = 0.8f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Energy")
		float ChargeStep = 0.1f;

	UFUNCTION()
		void Move(const FInputActionValue& InputActionValue);
	UFUNCTION()
		void Look(float DeltaSeconds);
	UFUNCTION()
		void Zoom(const FInputActionValue& InputActionValue);
	UFUNCTION()
		void Aim(const FInputActionValue& InputActionValue);
	UFUNCTION()
		void Walk(const FInputActionValue& InputActionValue);
	UFUNCTION()
		void FastRun(const FInputActionValue& InputActionValue);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();
	UFUNCTION(BlueprintCallable)
		void EnergyChange();

};

