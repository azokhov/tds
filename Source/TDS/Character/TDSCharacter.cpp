// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Kismet/GameplayStatics.h"
#include <Kismet/KismetMathLibrary.h>

ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	Look(DeltaSeconds);
}

void ATDSCharacter::BeginPlay()
{
	// Call the base class
	Super::BeginPlay();

	//Add Input Mapping Context

	if (APlayerController* PC = Cast<APlayerController>(GetController()))
	{
		// Get the Enhanced Input Local Player Subsystem from the Local Player related to our Player Controller.
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PC->GetLocalPlayer()))
		{
			Subsystem->ClearAllMappings();
			Subsystem->AddMappingContext(CharacterMappingContext, 0);
		}
	}
	GetWorldTimerManager().SetTimer(EnergyChargeTimer, this, &ATDSCharacter::EnergyChange, RateEnergyCharge, true);
	ChangeMovementState();
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	UEnhancedInputComponent* Input = Cast<UEnhancedInputComponent>(NewInputComponent);

	Input->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ATDSCharacter::Move);
	Input->BindAction(ZoomAction, ETriggerEvent::Triggered, this, &ATDSCharacter::Zoom);
	Input->BindAction(AimAction, ETriggerEvent::Started, this, &ATDSCharacter::Aim);
	Input->BindAction(AimAction, ETriggerEvent::Completed, this, &ATDSCharacter::Aim);
	Input->BindAction(WalkAction, ETriggerEvent::Started, this, &ATDSCharacter::Walk);
	Input->BindAction(WalkAction, ETriggerEvent::Completed, this, &ATDSCharacter::Walk);
	Input->BindAction(FastRunAction, ETriggerEvent::Started, this, &ATDSCharacter::FastRun);
	Input->BindAction(FastRunAction, ETriggerEvent::Completed, this, &ATDSCharacter::FastRun);

}

void ATDSCharacter::Move(const FInputActionValue& InputActionValue)
{
	if (InputActionValue.Get<FVector2D>() == FVector2D::Zero()) { return; }
	AddMovementInput(FVector::ForwardVector, InputActionValue.Get<FVector2D>().Y);
	AddMovementInput(FVector::RightVector, InputActionValue.Get<FVector2D>().X);
}

void ATDSCharacter::Look(float DeltaSeconds)
{
	if (MovementState == EMovementState::FastRun_State) { return; }
	APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (PC)
	{
		FHitResult HitResult;

		PC->GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, false, HitResult);

		FRotator NewRotator = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location);

		SetActorRotation(FRotator(0.0f, NewRotator.Yaw, 0.0f));


		/*	FVector WorldDirection, WorldLocation;
			WorldLocation = GetActorLocation();
			PC->DeprojectMousePositionToWorld(WorldLocation, WorldDirection);
			const FRotator MouseRotation = WorldDirection.Rotation();
			SetActorRotation(FRotator(0.0f, MouseRotation.Yaw, 0.0f));*/

	}
}

void ATDSCharacter::Zoom(const FInputActionValue& InputActionValue)
{
	float CameraHeight = CameraBoom->TargetArmLength;
	float ZoomValue = InputActionValue.Get<float>() * ZoomStep;
	float NewCameraHeight = CameraHeight + ZoomValue;
	float DeltaTime = UGameplayStatics::GetWorldDeltaSeconds(GetWorld());


	/*if (NewCameraHeight >= CameraHeightMin && CameraHeightMax >= NewCameraHeight)
	{
		CameraBoom->TargetArmLength = NewCameraHeight;
	}*/

	NewCameraHeight = UKismetMathLibrary::Clamp(NewCameraHeight, CameraHeightMin, CameraHeightMax);
	CameraBoom->TargetArmLength = UKismetMathLibrary::FInterpTo(CameraHeight, NewCameraHeight, DeltaTime, Smooth);
}

void ATDSCharacter::Aim(const FInputActionValue& InputActionValue)
{
	bIsAim = InputActionValue.Get<bool>();
	ChangeMovementState();
}

void ATDSCharacter::Walk(const FInputActionValue& InputActionValue)
{
	bIsWalk = InputActionValue.Get<bool>();
	ChangeMovementState();
}

void ATDSCharacter::FastRun(const FInputActionValue& InputActionValue)
{
	bIsFastRun = InputActionValue.Get<bool>();
	ChangeMovementState();
}

void ATDSCharacter::CharacterUpdate()
{
	//float ResSpeed = GetCharacterMovement()->MaxWalkSpeed;
	float ResSpeed = MovementInfo.RunSpeed;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::FastRun_State:
		ResSpeed = MovementInfo.FastRunSpeed;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDSCharacter::ChangeMovementState()
{
	if (!bIsAim && !bIsWalk && !bIsFastRun || Energy <= ChargeStep)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if (bIsAim)
		{
			MovementState = EMovementState::Aim_State;
		}
		if (bIsWalk && !bIsAim)
		{
			MovementState = EMovementState::Walk_State;
		}
		if (bIsFastRun && !bIsAim && !bIsWalk && Energy >= EnergyEnough)
		{
			MovementState = EMovementState::FastRun_State;
		}
	}
	CharacterUpdate();
}

void ATDSCharacter::EnergyChange()
{
	if (MovementState == EMovementState::FastRun_State && GetVelocity() != FVector::Zero())
	{
		Energy = Energy - ChargeStep;
		if (Energy <= ChargeStep)
		{
			ChangeMovementState();
		}
	}
	else
	{
		if (Energy < MaxEnergy)
		{
			Energy = Energy + ChargeStep;
			if (Energy >= EnergyEnough)
			{
				ChangeMovementState();
			}
		}
		else { return; }
	}
}
